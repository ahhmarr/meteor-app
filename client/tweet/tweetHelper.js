Template.tweetList.events({
	'submit #addTweet': function (e) {
		e.preventDefault();
		var elm=$(e.target);
		var newTweet={
			tweet : elm.find('[name=tweet]').val()
		}
		console.log(newTweet);
		Meteor.call('insertTweet',newTweet,()=>{
			g.s('created a new tweet !!');
			elm.find('[name=tweet]').val('');
		});
	},
	'click .edit': function(e){
		e.preventDefault();
		console.log(this._id);
		$('#editTweet').val(this.tweet);
		$('#editTweet').attr('data-id',this._id);
		$('.modal').modal('show');
	},
	'submit #editTweetForm':function(e){
		e.preventDefault();
		var elm=$(e.target).find('#editTweet');
		var tweet=elm.val();
		var id=elm.attr('data-id');
		Meteor.call('updateTweet',{id,tweet},function(e)
		{
			g.i('updated');
			$('.modal').modal('hide');
		});
	},
	'click .delete' : function(e)
	{
		e.preventDefault();
		var id=this._id;
		Meteor.call('deleteTweet',{id},function()
		{
			g.s('deleted successfully');
		});
	}

});
Template.tweetList.helpers({
	all: function () {
		return Tweets.find();
	},
	formatDate : function(d){
		var date=moment(d);
		// console.log(date);
		return date.format('DD-MMM-Y');
	}
});
Meteor.subscribe('allTweets');