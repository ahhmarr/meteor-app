Template.login.events({
	'submit #loginForm': function (e) {
		e.preventDefault();
		var user=getUserDetails(e);
		console.log(user);
		//creating user
		Meteor.loginWithPassword(user.username,user.password,(e)=>{
			if(e){
				Bert.alert(e.message,'danger');
				return;
			}
			// Bert.alert('welcome','success');
			g.i('welcome');
			FlowRouter.go('dashboard');
		});
	}
});
Template.register.events({
	'submit #registerForm' : function(e){
		e.preventDefault();
		var data=(getFormData($(e.target)));
		Meteor.call('register', data, function (error, result) {
			if(error){
				// console.log(error);
				Bert.alert(error.message,'danger');
				return;
			}
			// console.log('going to log in');
			Bert.alert('created user successfully','success');
			FlowRouter.go('login');
			// Meteor.loginWithPassword(data.username,data.password);
		});
	}
});
var getUserDetails=(e)=>{
	var elm=$(e.target);
	return {
		username : elm.find('[name=username]').val(),
		password : elm.find('[name=password]').val()
	};
}
var getFormData=($form)=>{
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}