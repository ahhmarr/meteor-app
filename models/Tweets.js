Tweets=new Mongo.Collection('tweets');
var TweetsSchema=new SimpleSchema({
	'tweet' : {
		type : String,
		label: 'tweet'
	},
	'author' : {
		type : String,
		optional : true,
		label 	: 'author name'
	},
	'created_at' : {
		type : Date,
		autoValue : function()
		{
			return new Date();
		}
	},
	'updated_at':{
		type : Date,
		optional :true
	}
});
Tweets.attachSchema(TweetsSchema);