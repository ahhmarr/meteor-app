//authentication routes
var auth=FlowRouter.group('/auth',{
	name : 'authentication',
	prefix : '/auth'
});
//restricted routes only to be used by users
var secure=FlowRouter.group({
	triggersEnter : [function(){
			if(!Meteor.userId()){
				g.e('access denied');
				FlowRouter.go('login');
			}
		}]
});
FlowRouter.route('/',{
	name	: 'home',
	action	: function(req,quer)
	{
		BlazeLayout.render('master',{
			yield : 'homepage'
		})
	}
});
secure.route('/tweets',{
	name : 'view-tweets',
	action : (req,quer)=>{
		BlazeLayout.render('master',{
			yield : 'tweetList'
		})
	}
});

FlowRouter.route('/login',{
	name : 'login',
	action : function(req,quer){
		BlazeLayout.render('master',{yield:'login'});
	}
});
auth.route('/register',{
	name : 'register',
	action : function(req,quer){
		BlazeLayout.render('master',{yield:'register'});
	}
});
auth.route('/logout',{
	name : 'logout',
	action : function(req,quer){
		Meteor.logout();
		Bert.alert('logged out ','info');
		FlowRouter.go('login');
	}
});
secure.route('/dashboard',{
	name : 'dashboard',
	action(req,res){
		BlazeLayout.render('master',{
			yield : 'dashboard'
		})
	}
})