Meteor.methods({
	'insertTweet' : function(d)
	{
		check(d,Object);
		Tweets.insert({
			'tweet' : d.tweet
		});
		console.log('called method');
	},
	'updateTweet': function(inp){
		check(inp,Object);
		console.log(inp.tweet);
		Tweets.update(
			{
				_id : inp.id
			},
			{
				$set : {
					tweet : inp.tweet,
					updated_at : new Date()
			 }
			}
		);
	},
	'deleteTweet' : function(inp){
		check(inp,Object);
		check(inp.id,String);	
		Tweets.remove({
			_id : inp.id
		});
	}
});
Meteor.publish('allTweets',function()
{
	return Tweets.find({},{sort : {created_at:-1}});
})