Meteor.methods({
	register: function (inp) {
		check(inp,Object);
		check(inp.username,String);
		Accounts.createUser(inp);
		// throwValidationError('!!!___!!!');
	}
});
var throwError=(msg,title='validation error')=>{
	throw new Meteor.Error(500,title,msg);
};